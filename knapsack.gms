* ****************************************
* IMPLEMENTATION OF THE KNAPSACK PROBLEM
* Giovanni Pantuso gp@math.ku.dk
* ****************************************
SETS
i 'Set of items' /1*100/
;


PARAMETER r(i) 'Reward of item i';
r(i) = uniform(100,120);

PARAMETER w(i) 'Weight of item i';
w(i) = normal(60,7);

SCALAR B 'Capacity of knapsack' /450/;

VARIABLES
z        'total reward'
x(i)   'if item i goes in the knapsack';

INTEGER VARIABLE x;

EQUATIONS
reward                'the equation of the total reward'
knap		      'the knapsack capacity';


reward..                   z =e= sum(i, r(i)*x(i));
knap..                 sum(i, w(i)*x(i)) =l= B;



OPTION reslim = 1e10;
OPTION limcol = 2;
OPTION limrow = 2;

MODEL knapsack /all/;
SOLVE knapsack USING mip MAXIMIZING z;

DISPLAY
         x.l
         z.l;




