* ****************************************
* IMPLEMENTATION OF THE CUTTING STOCK PROBLEM
* Giovanni Pantuso gp@math.ku.dk
* ****************************************


SET
i 'Set of small rolls sizes' /i1*i3/
;

PARAMETER w(i) 'width of small roll i'
	  /i1 2.1
           i2 1.8
           i3 1.5/;

PARAMETER b(i) 'demand of small roll i'
	  /i1 9
	   i2 12
	   i3 19/
	   ;

SCALAR D 'Width of large rolls' /5/;

* In order to implement the pattern-based formulation
* of the cutting stock problem, first we need to generate the set of patterns.
* How many possible patterns do we have?
* Given that the large rolls have width 5, from each large roll we can cut
* -- 0, 1, or 2 rolls i1
* -- 0, 1, or 2 rolls i2
* -- 0, 1, 2, or 3 rolls i3
* thus in principle, 3*3*4 = 36 rolls.
* However, not all of these are feasible. Therefore,
* we create a set of patterns, then we verify in a loop
* which ones are feasible. The feasible ones are assigned to a dynamic set.


SET p 'Set of cutting patterns' /p1*p36/;

TABLE Q(i,p) 'number of small rolls i cut in pattern p'
      p1  p2  p3  p4  p5  p6  p7  p8  p9  p10  p11  p12 p13  p14  p15  p16  p17  p18  p19  p20  p21  p22 p23  p24  p25  p26  p27  p28  p29  p30  p31  p32  p33  p34  p35 p36
   i1 0   0   0   0   0   0   0   0    0    0    0    0  1    1    1   1     1    1    1   1    1    1    1    1    2    2    2   2     2    2    2    2    2    2    2    2
   i2 0   0   0   0   1   1   1   1    2    2    2    2  0    0    0   0     1    1    1   1    2    2    2    2    0    0    0   0     1    1   1     1    2    2    2    2
   i3 0   1   2   3   0   1   2   3    0    1    2    3  0    1    2   3     0    1    2    3   0    1    2    3    0    1    2    3    0    1    2    3    0     1   2    3


SET feasP(P) 'Set of feasible patterns';

loop(p,
if(sum(i,w(i)*Q(i,p)) <= D,
	feasP(p) = yes;
	);
);



VARIABLES
z	'total number of rolls cut with any pattern'
x(p) 'the number of large rolls cut according to pattern p';
*INTEGER VARIABLE x;
POSITIVE VARIABLE x;
EQUATIONS
n_rolls		'the total number of large rolls cut with any patter'
demand(i) 		'the demand of small rolls'
;

n_rolls..	z =e= sum(p$(feasP(p)),x(p));
demand(i)..	sum(p$(feasP(p)),Q(i,p)*x(p)) =g= b(i);


OPTION reslim = 1e10;
OPTION limcol = 2;
OPTION limrow = 2;

MODEL cs /all/;
*SOLVE cs USING mip MINIMIZING z;
SOLVE cs USING lp MINIMIZING z;

DISPLAY
feasP
x.l
z.l
;




