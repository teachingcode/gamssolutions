* ****************************************
* IMPLEMENTATION OF THE MATCHING PROBLEM
* Giovanni Pantuso gp@math.ku.dk
* ****************************************
SETS
i 'index of people' /1*10/
;
alias(i,j);


PARAMETER r(i,j) 'quality of match i-j';
r(i,j)$(ord(i) < ord(j)) = uniform(40,80);
r(i,j)$(ord(j) < ord(i)) = r(j,i);

VARIABLES
z        'total quality'
x(i,j)   'assignment of i to j';

BINARY VARIABLE x;

EQUATIONS
quality                'the equation of the total quality'
match(i)               'each person must match exactly one other person';


quality..                   z =e= sum((i,j)
                                 $(ord(i) lt ord(j)), r(i,j)*x(i,j));
match(i)..                 sum(j$(ord(j) lt ord(i)), x(j,i))
                           + sum(j$(ord(j) gt ord(i)), x(i,j))
                                                         =e= 1;



OPTION reslim = 1e10;
OPTION limcol = 2;
OPTION limrow = 2;

MODEL matching /all/;
SOLVE matching USING mip MAXIMIZING z;

DISPLAY
	r	
         x.l
         z.l;




