* ****************************************
* IMPLEMENTATION OF THE CUTTING STOCK PROBLEM
* Giovanni Pantuso gp@math.ku.dk
* ****************************************
SET
i 'Set of small rolls sizes' /i1*i3/
;


PARAMETER w(i) 'width of small roll i'
	  /i1 2.1
           i2 1.8
           i3 1.5/;

PARAMETER b(i) 'demand of small roll i'
	  /i1 9
	   i2 12
	   i3 19/
	   ;

SCALAR D 'Width of large rolls' /5/;

* Note that a (loose) upper bound on the number of large rolls
* needed is the sum of all the demands.
SET
j 'Set of large rolls' /r1*r40/;

VARIABLES
z        'number of large rolls used'
y(j)     'if large roll j is used'
x(i,j)	 'the number of small rolls of type i are cut from large roll j';

*INTEGER VARIABLE x;
*BINARY VARIABLE y;
POSITIVE VARIABLE x;
POSITIVE VARIABLE y;
y.up(j) = 1;

EQUATIONS
n_rolls                'the equation of the total number of large rolls'
with(j)		      'the equation on the width of the large rolls'
demand(i)		      'the equation on the demand of the small rolls'
antisymmetry(j)		      'Use roll j only if j-1 was used';

n_rolls..                   z =e= sum(j, y(j));
with(j)..                 sum(i, w(i)*x(i,j)) =l= D*y(j);
demand(i)..		  sum(j, x(i,j)) =g= b(i);
antisymmetry(j)..	  y(j)$(ord(j) > 1) =l= y(j-1);



OPTION reslim = 1e10;
OPTION limcol = 2;
OPTION limrow = 2;

MODEL cs /all/;
*SOLVE cs USING mip MINIMIZING z;
SOLVE cs USING lp MINIMIZING z;

DISPLAY
	z.l
	y.l
         x.l;




