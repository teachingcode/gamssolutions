* ****************************************
* IMPLEMENTATION OF THE ASSIGNMENT PROBLEM
* Giovanni Pantuso gp@math.ku.dk
* ****************************************
SETS
i "Set of people" /i1*i8/
j "Set of jobs" /j1*j5/
;



PARAMETER C(i,j) "Cost of assigning i to j"
;
C(i,j) = normal(50,15);

VARIABLES
z        "Total cost"
x(i,j)   "Assignment of i to j";

* Check that you can solve it as an LP
*BINARY VARIABLE x;
POSITIVE VARIABLE x;
x.up(i,j) = 1;


EQUATIONS
cost                 "The total cost equation"
match(i)               "each i to at most one j"
exactlyOne(j)	       "each j exactly one i"
;


cost..                   z =e= sum((i,j), c(i,j)*x(i,j) );
match(i)..                 sum(j, x(i,j))    =l= 1;
exactlyOne(j)..		   sum(i, x(i,j))    =e= 1;


OPTION reslim = 1e10;
OPTION limcol = 2;
OPTION limrow = 2;

MODEL assignment /all/;
* Use mip if solving it as an IP, lp otherwise
*SOLVE assignment USING mip MINIMIZING z;
SOLVE assignment USING lp MINIMIZING z;

DISPLAY
         x.l
         z.l;




